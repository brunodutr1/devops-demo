package com.github.brunodutr.devopsdemo.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("hello")
public class HelloController {

	private final static Logger logger = LoggerFactory.getLogger(HelloController.class);
	
	@GetMapping
	public String hello(@RequestParam String name) {
	
		logger.info("Called with parameter: {}", name);
		
		return "Hello "+ name;
	
	}
}
