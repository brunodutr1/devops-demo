package com.github.brunodutr.devopsdemo.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.brunodutr.devopsdemo.dto.HealthDTO;

@RestController
@RequestMapping("health")
public class HealthController {

	@GetMapping
	public HealthDTO status() {
		return new HealthDTO("up");
	}
}
