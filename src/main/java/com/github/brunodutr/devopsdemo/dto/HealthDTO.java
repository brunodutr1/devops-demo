package com.github.brunodutr.devopsdemo.dto;


public class HealthDTO {

	private String serverStatus;

	public HealthDTO(String serverStatus) {
		this.serverStatus = serverStatus;
	}

	public String getServerStatus() {
		return serverStatus;
	}

	public void setServerStatus(String serverStatus) {
		this.serverStatus = serverStatus;
	}
}
