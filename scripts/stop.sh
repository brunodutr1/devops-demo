#!/bin/bash

ps -ef | grep devops-demo.war | grep -v grep | awk '{print $2}' | xargs kill